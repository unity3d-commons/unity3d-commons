using UnityEngine;

public class Projectile : MonoBehaviour
{
    private float movementSpeed = 6f;
    [SerializeField] private string ProjetileTag;

    private Rigidbody2D _rigidbody2D;

    void Start()
    {
        _rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        if (ProjetileTag == "EnemyProjectile")
        {
            _rigidbody2D.velocity = Vector2.down * movementSpeed;
        }
        else if (ProjetileTag == "PlayerProjectile")
        {
            _rigidbody2D.velocity = Vector2.up * movementSpeed;
        }
    }

    void Awake()
    {
        Destroy(gameObject, 2f);
    }

    void Update()
    {
        // Move Enemy ship towards player
        // float newY = transform.position.y + (movementSpeed * Time.deltaTime);
        // Vector3 newPost = new Vector3(transform.position.x, newY, 0.0f);
        // transform.position = newPost;

        // Destroy projectile once it passes beyond screen
        // if (ProjetileTag == "EnemyProjectile")
        // {
        //     if (transform.position.y <= selfDestructionPosition)
        //     {
        //         Debug.Log("Destroying: " + ProjetileTag);
        //         Destroy(gameObject);
        //     }
        // }
        // else
        // {
        //     if (transform.position.y >= selfDestructionPosition)
        //     {
        //         Debug.Log("Destroying: " + ProjetileTag);
        //         Destroy(gameObject);
        //     }
        // }
    }
}
using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float movementSpeed = 5f;
    [SerializeField] private float enemySwingSpeed = 5f;
    [SerializeReference] float selfDestructionPosition = -7.0f;
    [SerializeField] private GameObject projetile;
    Quaternion enemyRotation = Quaternion.Euler(0.0f, 0.0f, -90.0f);
    private  float sign; 
    protected float m_WaveAmplitude;
    protected Vector2 m_Direction = Vector2.left;

    private void Start()
    {
        m_WaveAmplitude = Random.Range(-2.35f, 6);

        tag = "Enemy";
        sign = Random.value > 0.5f ? 1.0f : -1.0f;
        StartCoroutine(SpawnProjectile());
        
    }

    void Update()
    {
        
        // Move Enemy ship towards player
        float newY = transform.position.y + (-movementSpeed * Time.deltaTime);
        float newX = Mathf.Sin(Time.time * m_WaveAmplitude) * 2.5f;
        // float newX = transform.position.x * sign + ((Random.Range(1f, enemySwingSpeed))  * Time.deltaTime);
        Vector3 newPost = new Vector3(newX*m_Direction.magnitude, newY, 0.0f);
        // Vector3 newPost = new Vector3(newX, newY, 0.0f);
        transform.position = newPost;
        // transform.Translate(m_Direction * -movementSpeed * Time.deltaTime);
        // Fire projectile
        // Destroy Enemy ship once it passes player's ship
        if (transform.position.y <= selfDestructionPosition)
        {
            Destroy(gameObject);
        }
    }
    
    
    IEnumerator SpawnProjectile()
    {
        Vector3 positionPadding = new Vector3(0.0f, -0.2f, 0.0f); 
        for (int i = 0; i < Int32.MaxValue; i++)
        {
            Instantiate(projetile, transform.position + positionPadding, enemyRotation);
            yield return new WaitForSeconds(getRandomSpwanTime());
        }
    }

    float getRandomSpwanTime()
    {
        return Random.Range(-0.2f, 1.0f);
    }

    public void KillEnemy()
    {
        Destroy(gameObject);
    }
}
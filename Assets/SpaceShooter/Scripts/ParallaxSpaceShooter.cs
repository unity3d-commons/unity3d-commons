using UnityEngine;
using UnityEngine.Serialization;

public class ParallaxSpaceShooter : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;

    [FormerlySerializedAs("scrollLeft")] [SerializeField]
    private bool scrollUp;

    private float singleTextureWidth;


    // Start is called before the first frame update
    void Start()
    {
        SetupTexture();
        if (scrollUp)
        {
            moveSpeed = -moveSpeed;
        }
    }

    void SetupTexture()
    {
        // spriteRenderer = GetComponent<SpriteRenderer>();
        // spriteRenderer.sprite = sprite;
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        Sprite sprite = spriteRenderer.sprite;
        singleTextureWidth = sprite.texture.height / sprite.pixelsPerUnit;
        Debug.Log("Texture width: " + singleTextureWidth);
    }

    // Update is called once per frame
    void Update()
    {
        Scroll();
        CheckReset();
    }

    void Scroll()
    {
        float delta = moveSpeed * Time.deltaTime;
        transform.position += new Vector3(0f, delta, 0f);
    }

    void CheckReset()
    {
        float currentPosition = Mathf.Abs(transform.position.y) - singleTextureWidth;
        if (currentPosition > 0)
        {
            transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);
        }
    }
}
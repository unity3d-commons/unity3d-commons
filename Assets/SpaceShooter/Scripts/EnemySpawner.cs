using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject enemyRed;
    private Vector3 spawnPosition;
    Quaternion enemyRotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);

    void Start()
    {
        spawnPosition = new Vector3(Random.Range(-2.35f, 2.35f), 5.5f, 0);

        StartCoroutine(SpawnEnemyRed());
    }

    IEnumerator SpawnEnemyRed()
    {
        for (int i = 0; i < 10; i++)
        {
            spawnPosition = new Vector3(Random.Range(-2.35f, 2.35f), 5.5f, 0);
            Instantiate(enemyRed, spawnPosition, enemyRotation, transform);
            yield return new WaitForSeconds(getRandomSpwanTime());
        }
    }

    float getRandomSpwanTime()
    {
        return 2f;
    }
}
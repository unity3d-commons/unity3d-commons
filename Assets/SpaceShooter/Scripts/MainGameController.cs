using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class MainGameController : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private GameObject btnHost;
    [SerializeField] private GameObject btnClient;
    [SerializeField] private GameObject btnQuit;
    
    public void OnStartHost()
    {
        Debug.Log("Start host");
        if (NetworkManager.Singleton == null)
        {
            Debug.Log("No Network manager present, cannot start as host");
            return;
        }
        
        NetworkManager.Singleton.StartHost();
        btnHost.SetActive(false);
        btnClient.SetActive(false);
        btnQuit.SetActive(true);
    }

    public void OnStartClient()
    {
        Debug.Log("Start Client");
        
        if (NetworkManager.Singleton == null)
        {
            Debug.Log("No Network manager present, cannot start as client");
            return;
        }
        NetworkManager.Singleton.StartClient();
        btnHost.SetActive(false);
        btnClient.SetActive(false);
        btnQuit.SetActive(true);
    }
    
    public void OnQuit()
    {
       Application.Quit();
    }
}
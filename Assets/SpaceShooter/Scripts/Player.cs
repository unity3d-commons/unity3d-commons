using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    [SerializeField] private int life = 100;
    [SerializeField] private GameObject playerProjectile;
    [SerializeField] float movementSpeed = 5f;
    [SerializeField] float playerPaddingTop = 0.5f;
    [SerializeField] float playerPaddingRight = 0.5f;
    [SerializeField] float playerPaddingBottom = 2f;
    [SerializeField] float playerPaddingLeft = 0.5f;
    private Vector2 userInput;

    private Vector2 minBound;

    private Vector2 maxBound;
    
    Vector3 positionPadding = new Vector3(0.0f, 0.2f, 0.0f); 


    // Start is called before the first frame update
    void Start()
    {
        Camera camera = Camera.main;
        minBound = camera.ViewportToWorldPoint(new Vector2(0, 0));
        maxBound = camera.ViewportToWorldPoint(new Vector2(1, 1));
    }

    void OnMove(InputValue inputValue)
    {
        userInput = inputValue.Get<Vector2>();
    }

    void OnFire(InputValue inputValue)
    {
        Debug.Log("User fired projectile");
        Instantiate(playerProjectile, transform.position + positionPadding, Quaternion.Euler(0.0f, 0.0f, 90.0f));
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 delta = userInput * (movementSpeed * Time.deltaTime);
        Vector2 newPosition = new Vector2();
        newPosition.x = Mathf.Clamp(transform.position.x + delta.x, minBound.x + playerPaddingLeft,
            maxBound.x - playerPaddingRight);
        newPosition.y = Mathf.Clamp(transform.position.y + delta.y, minBound.y + playerPaddingBottom,
            maxBound.y - playerPaddingTop);
        transform.position = newPosition;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Enemy enemy = other.GetComponent<Enemy>();
        Debug.Log("Damaged Player from: " + other.tag);
        // enemy.KillEnemy();
        string tag = other.tag;
        Debug.Log("Other collider: " + tag);
        if (tag == "EnemyProjectile")
        {
            ProjectileDamage damage = other.GetComponent<ProjectileDamage>();
            life = life - damage.GetDamage();
        }

        if (life <= 0)
        {
            Debug.Log("Played died");
            Destroy(gameObject);
        }
    }
}
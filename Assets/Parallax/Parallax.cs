using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private bool scrollLeft;

    private float singleTextureWidth;


    // Start is called before the first frame update
    void Start()
    {
        SetupTexture();
        if (scrollLeft)
        {
            moveSpeed = -moveSpeed;
        }
    }

    void SetupTexture()
    {
        // spriteRenderer = GetComponent<SpriteRenderer>();
        // spriteRenderer.sprite = sprite;
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        Sprite sprite = spriteRenderer.sprite;
        singleTextureWidth = sprite.texture.width / sprite.pixelsPerUnit;
        Debug.Log("Texture width: " + singleTextureWidth);
    }

    // Update is called once per frame
    void Update()
    {
        Scroll();
        CheckReset();
    }

    void Scroll()
    {
        float delta = moveSpeed * Time.deltaTime;
        transform.position += new Vector3(delta, 0f, 0f);
    }

    void CheckReset()
    {
        float currentPosition = Mathf.Abs(transform.position.x) - singleTextureWidth;
        if (currentPosition > 0)
        {
            transform.position = new Vector3(0.0f, transform.position.y, transform.position.z);
        }
    }
}